/*
 * XMLOperations.cpp
 *
 *  Created on: 10-01-2014
 *      Author: Casper Schou
 */


#include <vector>
#include <iostream>
#include <QObject>
#include <qtextstream.h>
#include <QVector>
#include <QString>
#include <QDomElement>
#include <QList>
#include <QFile>
#include <QFileInfo>

#include <xml_operations/xml_operations.hpp>
#include <logger_sys/logger_sys.hpp>

typedef boost::shared_ptr<QDomElement> p_element;

using namespace std;

//Parameters - These are placed here instead of the header to avoid including QT-files in the header...


XMLOperations::XMLOperations(bool verbose)
{
    FDEBUG("XMLOperations::XMLOperations");
    verbose_ = verbose;

    xml_file_ = new QFile();
    dom_document_ = new QDomDocument();
    current_tag_ = new QDomElement();
}

XMLOperations::~XMLOperations()
{
    closeFile();
}

bool XMLOperations::openFile(string fullFilePath)
{
    //First check if another file is already open...
    if(xml_file_->isOpen()) //another file IS open
    {
        FWARN("Another file is currently open. Closing it...");
        closeFile();
    }

    //Check if the file exists:
    xml_file_->setFileName(QString::fromStdString(fullFilePath));
    if(!xml_file_->exists())
    {
        FERROR("File: " << fullFilePath << " does not seem to exist.");
        return false;
    }

    //Open the file
    if(!xml_file_->open(QIODevice::ReadOnly))
    {
        FERROR("Failed to open file: " << fullFilePath);
        xml_file_->close();
        return false;
    }

    //Read in the content
    QString errorStr;
    int errorLine;
    int errorColumn;

    if (!dom_document_->setContent(xml_file_, &errorStr, &errorLine, &errorColumn))
    {
        FERROR("XML read: " << "Parse error at line " << errorLine << ", column " <<
               errorColumn << ":" << endl << errorStr.toStdString());

        return false;
    }

    *current_tag_ = dom_document_->documentElement(); //set to root!

    return true;
}

bool XMLOperations::openFile(string path, string file)
{
    //Put path and file together
    size_t found = path.rfind("/");
    if(found < (path.size()-1))         //IF the path does not end with a "/" - add it!
    {
        path = path + "/";
    }

    string fullFilePath = path + file;

    return openFile(fullFilePath);
}

void XMLOperations::initialize(string root_tag_name)
{
    dom_document_->setContent(QString("<?xml version=\"1.0\" encoding=\"utf-8\" ?>"),false);

    QDomElement root = dom_document_->createElement(QString::fromStdString(root_tag_name));
    dom_document_->appendChild(root);

    *current_tag_ = dom_document_->documentElement();
}

bool XMLOperations::writeFile()
{
    //if no file is associated with the document, simply ignore
    if(xml_file_->fileName() == "")
        return true;

    //Store the filename temp.
    QString filename = xml_file_->fileName();

    //close the file.
    xml_file_->close();

    //reopen and truncate
    if(!xml_file_->open(QFile::WriteOnly | QFile::Truncate))
    {
        FERROR("Failed to open file for writing: " << filename.toStdString() << " Trying to reload the object...");

        xml_file_->close();

        if(openFile(filename.toStdString()))
        {
            if(verbose_)
                FINFO("Successfully reloaded the file...");
        }
        else
        {
            FERROR("Failed to reload file!");

            dom_document_->clear();
            current_tag_->clear();
            xml_file_->setFileName("");
        }
        return false;
    }

    QTextStream stream;
    stream.setDevice(xml_file_);
    stream << dom_document_->toString();

    //Close and reopen in read mode
    xml_file_->close();
    if(!xml_file_->open(QIODevice::ReadOnly))
    {
        FERROR("Failed to re-open file: " << filename.toStdString() << " in read mode!");

        dom_document_->clear();
        current_tag_->clear();
        xml_file_->close();
        xml_file_->setFileName("");
        return false;
    }

    return true;
}

void XMLOperations::closeFile()
{
    dom_document_->clear();
    current_tag_->clear();
    xml_file_->close();
    xml_file_->setFileName("");
}

bool XMLOperations::createNew(string root_tag_name)
{
    //First check if another file is already open...
    if(xml_file_->isOpen()) //another file IS open
    {
        FWARN("Another file is currently open. Closing it...");
        closeFile();
    }

    //to be absolutely sure no file is associated:
    xml_file_->setFileName("");

    initialize(root_tag_name);

    return true;
}

bool XMLOperations::createNewFile(string full_file_path, string root_tag_name)
{
    //First check if another file is already open...
    if(xml_file_->isOpen()) //another file IS open
    {
        FWARN("Another file is currently open. Closing it...");
        closeFile();
    }

    //Check if the file exists:
    xml_file_->setFileName(QString::fromStdString(full_file_path));
    if(xml_file_->exists())
    {
        FERROR("File: " << full_file_path << " already exists.");
        return false;
    }

    initialize(root_tag_name);

    return writeFile();
}

bool XMLOperations::createNewFile(string path, string filename, string root_tag_name)
{
    //Put path and file together
    size_t found = path.rfind("/");
    if(found < (path.size()-1))         //IF the path does not end with a "/" - add it!
    {
        path = path + "/";
    }

    //check filename for ending:
    found = filename.rfind(".");
    if (found!=string::npos)            //extension found!
    {
        if(filename.substr(found+1) != "xml" && filename.substr(found+1) != "XML")
        {
            //other ending received. Remapping to xml:
            filename.erase(found);
            filename = filename + ".xml";
        }
    }
    else                                //no ending found - adding it!
    {
        filename = filename + ".xml";
    }

    string full_file_path = path + filename;

    return createNewFile(full_file_path, root_tag_name);
}

bool XMLOperations::deleteFile(string full_file_path)
{
    QFile marked_for_delete;

    if(full_file_path =="") //no file specified!
    {
        if(!xml_file_->isOpen()) //no file currently open
        {
            FERROR("Cannot delete file. No file open and no filename specified!");
            return false;
        }

        marked_for_delete.setFileName(xml_file_->fileName());
        closeFile();
    }
    else
    {

        //check if the provided filename is actually the file open (could happen)
        if(full_file_path == xml_file_->fileName().toStdString())
        {
            FDEBUG("Requested to delete the currently open file. File will be closed!")
                    closeFile();
        }

        marked_for_delete.setFileName(QString::fromStdString(full_file_path));
    }

    return marked_for_delete.remove();
}

bool XMLOperations::deleteFile(string path, string filename)
{
    //Put path and file together
    size_t found = path.rfind("/");
    if(found < (path.size()-1))         //IF the path does not end with a "/" - add it!
    {
        path = path + "/";
    }

    string full_file_path = path + filename;

    return deleteFile(full_file_path);
}

bool XMLOperations::clearFile(void)
{
    cout << "XMLOperations::clearFile" << endl;

    QDomElement root = dom_document_->documentElement();
    string root_tag_name = root.tagName().toStdString();
    cout << "XML root_tag_name=" << root_tag_name << endl;

    // Delete content of this class:
    dom_document_->clear();
    current_tag_->clear();

    initialize(root_tag_name);

    bool write_succes = writeFile();
    cout << "write completed:" << write_succes << endl;

    return write_succes;
}

bool XMLOperations::saveAs(string full_file_path, bool overwrite)
{
    //Check that a document indeed exists:
    if(dom_document_->isNull())
    {
        FDEBUG("No document currently in memory. Either use createNewFile or createNew");
        return false;
    }

    //Check if current file is opened:
    bool success = false;
    if (xml_file_->isOpen())
    {
        if (xml_file_->copy(QString::fromStdString(full_file_path)))
        {
            closeFile();
            openFile(full_file_path);
            success = true;
        }
        else
        {
            // Overwriting not implemented for open files yet...
            if(verbose_)
            {
                FERROR("File: " << full_file_path << " already exists. Not overwriting.");
            }
        }
    }
    else	// xml_file_ is not open
    {
        xml_file_->setFileName(QString::fromStdString(full_file_path));

        if(xml_file_->exists() && !overwrite)
        {
            if(verbose_)
            {
                FERROR("File: " << full_file_path << " already exists and overwrite not requested");
            }
        }
        else
        {
            success = writeFile();
        }
    }

    return success;
}

bool XMLOperations::saveAs(string path, string filename, bool overwrite)
{
    //Put path and file together
    size_t found = path.rfind("/");
    if(found < (path.size()-1))         //IF the path does not end with a "/" - add it!
    {
        path = path + "/";
    }

    //check filename for ending:
    found = filename.rfind(".");
    if (found!=string::npos)            //extension found!
    {
        if(filename.substr(found+1) != "xml" && filename.substr(found+1) != "XML")
        {
            //other ending received. Remapping to xml:
            filename.erase(found);
            filename = filename + ".xml";
        }
    }
    else                                //no ending found - adding it!
    {
        filename = filename + ".xml";
    }


    string full_file_path = path + filename;

    return saveAs(full_file_path,overwrite);
}

bool XMLOperations::isSaved()
{
    if(xml_file_->fileName() == "")
        return false;

    return true;
}

std::string XMLOperations::getFilenameFull(void) const
{
    if (xml_file_ == NULL || !xml_file_->exists())
    {
        FWARN("Attempting to get name of invalid xmlfile!");
        return "";
    }

    cout << "QString: " << xml_file_->fileName().toStdString() << endl;
    return xml_file_->fileName().toStdString();
}

std::string XMLOperations::getFilename(void) const
{
    std::string filename_full = getFilenameFull();
    if (filename_full == "")
        return filename_full;

    QFileInfo fileInfo(QString::fromStdString(filename_full));
    return fileInfo.fileName().toStdString();
}

//Searching the document:
bool XMLOperations::findTags(string tag)
{
    //Let's check, that a file is indeed already loaded/open:
    if(dom_document_->isNull())
    {
        FDEBUG("No file is currently loaded. Please load a file first using openFile.");
        return false;
    }

    QDomElement* root = new QDomElement(dom_document_->documentElement());

    QDomNodeList results = root->elementsByTagName(QString::fromStdString(tag));

    //clear the tags container:
    clearTagsContainer();

    //check the root tag itself
    if(root->tagName().toStdString() == tag)
        tags_container_.push_back(root);

    for(int i=0;i<results.size();i++)
    {
        tags_container_.push_back( new QDomElement(results.at(i).toElement()) );
    }

    return true;
}

bool XMLOperations::goToFirstMatch(string tag)
{
    if(!findTags(tag))
    {
        FDEBUG("Failed to search the document!");
        return false;
    }

    if(tags_container_.size() < 1)
        return false;

    *current_tag_ = *tags_container_[0];
    tags_container_current_index_ = 0;

    return true;
}

bool XMLOperations::goToNextMatch()
{
    //check that a search has actually been caried out!
    if(tags_container_.size() == 0)
    {
        FDEBUG("Couldn't go to next match. No search-results available. Are you sure a search has been performed and succeeded?");
        return false;
    }
    else if((int)tags_container_.size() == tags_container_current_index_ +1)
    {
        FDEBUG("Couldn't go to next match. End of list!");
        return false;
    }

    tags_container_current_index_++;
    *current_tag_ = *tags_container_.at(tags_container_current_index_);

    return true;
}

bool XMLOperations::goToMatch(string tag, unsigned int number)
{
    if(!findTags(tag))
    {
        FDEBUG("Failed to search the document!");
        return false;
    }

    if(number > tags_container_.size() || number < 1)
    {
        FDEBUG("Couldn't go to match. Out of range!");
        return false;
    }

    *current_tag_ = *tags_container_.at(number-1);
    tags_container_current_index_ = number-1;

    return true;
}


//Document navigation
bool XMLOperations::goToPath(string path)
{
    /* Assigns the local "current_tag_" to the specified tag specified from path
      * The specified path is the absolute path to the target tag.
      * "/" is used to denote a level drop
      * IF multiple children are of the same name, the function will take the first.
      */

    //Let's check, that a file is indeed already loaded/open:
    if(dom_document_->isNull())
    {
        FDEBUG("Could not go to tag: " << path << " because no file is currently loaded. Please load a file first using openFile.");
        return false;
    }

    QDomElement data, data1;
    data = dom_document_->documentElement(); //hence root!

    size_t found;
    string temp_tag;


    while(1)    //when a tag no longer contains "/", it is the "last" child-tag
    {
        found = path.find("/");

        if(found == 0)      //the tag starts with "/"
        {
            path.erase(0,1);
            found = path.find("/");
        }

        temp_tag = path;


        if(found != string::npos)
        {
            temp_tag.erase(found);
        }

        data1 = data;
        data = data1.firstChildElement(QString::fromStdString(temp_tag));

        if(data.isNull() == true)
        {
            if(verbose_)
            {FERROR("Could not locate tag: " << temp_tag);}
            else
            {FDEBUG("Could not locate tag: " << temp_tag);}
            return false;
        }

        if(found != string::npos)
        {
            //update the tag - hence, remove the already found part
            path.erase(0,found + 1);
        }
        else                //hence, we didn't find any more division, thus it must be the "last" child
        {
            break;
        }
    }

    *current_tag_ = data;

    return true;
}

bool XMLOperations::goToFirstChild(string child)
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    QDomElement temp;

    if(child == "")
        temp = current_tag_->firstChildElement();
    else
        temp = current_tag_->firstChildElement(QString::fromStdString(child));

	if(temp.isNull())
	{
		FDEBUG("Failed to go to first child with tag: " << child);
		return false;
	}

    *current_tag_ = temp;

    return true;
}

bool XMLOperations::goToNextSibling(string sibling)
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set!");
        return false;
    }

    QDomElement temp;

    if(sibling == "")
        temp = current_tag_->nextSiblingElement();
    else
        temp = current_tag_->nextSiblingElement(QString::fromStdString(sibling));

    if(temp.isNull())
    {
        if(sibling == "")
        {
            if(verbose_)
                FERROR("Tag: " << current_tag_->tagName().toStdString() << " has no more siblings!");
        }
        else
        {
            if(verbose_)
                FERROR("Tag: " << current_tag_->tagName().toStdString() << " has no more sibling named: " << sibling);
        }
        return false;
    }

    *current_tag_ = temp;

    return true;
}

bool XMLOperations::goToParent()
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    QDomElement temp = current_tag_->parentNode().toElement();

    if(temp.isNull())
    {
        FDEBUG("Tag: " << current_tag_->tagName().toStdString() << " has no parent! Hence, it must be root!");
        return false;
    }

    *current_tag_ = temp;

    return true;
}

bool XMLOperations::goUp(int steps)
{
    for (int i = 0; i < steps; i++)
    {
        if (!goToParent())
            break;
    }
}

bool XMLOperations::goToRoot()
{
    //Let's check, that a file is indeed already loaded/open:
    if(dom_document_->isNull())
    {
        FDEBUG("No file is currently loaded. Please load a file first using openFile.");
        return false;
    }

    *current_tag_ = dom_document_->documentElement();

    return true;
}

bool XMLOperations::goToTag(QDomElement* tag)
{
	//Let's check
	if(tag->isNull())
	{
		FDEBUG("Destination tag is NULL.");
		return false;
	}

	current_tag_ = tag;

	return true;
}


//Document search, count, info
unsigned int XMLOperations::countOccurances(string tag)
{
    //Let's check, that a file is indeed already loaded/open:
    if(dom_document_->isNull())
    {
        FDEBUG("No file is currently loaded. Please load a file first using openFile.");
        return 0;
    }

    QDomElement root = dom_document_->documentElement();

    QDomNodeList results = root.elementsByTagName(QString::fromStdString(tag));

    unsigned int count = (unsigned int)results.size();

    //check the root tag itself:
    if(root.tagName().toStdString() == tag)
        count++;

    return count;
}

bool XMLOperations::hasAttribute(string attr_name)
{
    return current_tag_->hasAttribute(QString::fromStdString(attr_name));
}

//Retrieve data
bool XMLOperations::getValue(XMLOp::XMLValue& value)
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    value.set(current_tag_->text().toStdString());

    return true;
}

XMLOp::XMLValue XMLOperations::getValue()
{
    XMLOp::XMLValue value;

    getValue(value);    //if fails, value is already init to ""

    return value;
}

bool XMLOperations::getTagName(string& tag)
{
    //Let's check, that current tag has been set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    tag = current_tag_->tagName().toStdString();

    return true;
}
string XMLOperations::getTagName(void)
{
    //Let's check, that a file is indeed already open:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    return current_tag_->tagName().toStdString();
}

bool XMLOperations::getElement(string& element)
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    if(!current_tag_->hasChildNodes())
    {
        FDEBUG("Tag: " << current_tag_->tagName().toStdString() << " has no children. Not possible to get an element! Use the getValue() function to get a single value.");
        return false;
    }

    element = current_tag_->text().toStdString();

    return true;
}

bool XMLOperations::getElement(QDomElement& element)
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    if(!current_tag_->hasChildNodes())
    {
        FDEBUG("Tag: " << current_tag_->tagName().toStdString() << " has no children. Not possible to get an element! Use the getValue() function to get a single value.");
        return false;
    }

    element = *current_tag_;

    return true;
}

bool XMLOperations::getChildren(map<string,XMLOp::XMLValue>& children)
{
    if(!goToFirstChild())
    {
        if(verbose_)
            FERROR("Failed to go to first child og tag: " << current_tag_->tagName().toStdString() << ". No children found!");
        return false;
    }

    XMLOp::XMLValue value;
    string tag;

    do
    {
        if(!getTagName(tag))
        {
            FDEBUG("Failed to get tag-name");
            continue;
        }
        if(!getValue(value))
        {
            FDEBUG("Failed to get value");
            continue;                           //Should make sure only values are added
        }

        children.insert(pair<string,XMLOp::XMLValue>(tag,value));

    }while(goToNextSibling());

    //return to parent:
    goToParent();

    return true;
}

bool XMLOperations::getChildren(map<string,int>& children)
{
    map<string,XMLOp::XMLValue> myMap;

    if(!getChildren(myMap)) return false;

    for(map<string,XMLOp::XMLValue>::iterator itr=myMap.begin(); itr != myMap.end();itr++)
    {
        children.insert(pair<string,int>(itr->first, itr->second.toInt()));
    }

    return true;
}

bool XMLOperations::getChildren(map<string,double>& children)
{
    map<string,XMLOp::XMLValue> myMap;

    if(!getChildren(myMap)) return false;

    for(map<string,XMLOp::XMLValue>::iterator itr=myMap.begin(); itr != myMap.end();itr++)
    {
        children.insert(pair<string,double>(itr->first, itr->second.toDouble()));
    }

    return true;
}

std::string XMLOperations::getParentTagName()
{
	string parent_tag = current_tag_->parentNode().toElement().tagName().toStdString();
	return parent_tag;
}

bool XMLOperations::getChildrenValues(std::vector<XMLOp::XMLValue> &children_values)
{
    map<string,XMLOp::XMLValue> myMap;

    if(!getChildren(myMap)) return false;

    for(map<string,XMLOp::XMLValue>::iterator itr=myMap.begin(); itr != myMap.end();itr++)
    {
        children_values.push_back(itr->second);
    }

    return true;
}

bool XMLOperations::getChildrenValues(std::vector<string> &children_values)
{
    map<string,XMLOp::XMLValue> myMap;

    if(!getChildren(myMap)) return false;

    for(map<string,XMLOp::XMLValue>::iterator itr=myMap.begin(); itr != myMap.end();itr++)
    {
        children_values.push_back(itr->second.toString());
    }

    return true;
}


bool XMLOperations::getChildren(map<string,string>& children)
{
    map<string,XMLOp::XMLValue> myMap;

    if(!getChildren(myMap)) return false;

    for(map<string,XMLOp::XMLValue>::iterator itr=myMap.begin(); itr != myMap.end();itr++)
    {
        children.insert(pair<string,string>(itr->first, itr->second.toString()));
    }

    return true;
}

int XMLOperations::getNumberOfChildren()
{
    map<string,XMLOp::XMLValue> myMap;

    if(!getChildren(myMap)) return 0;

    return myMap.size();
}

bool XMLOperations::getAttribute(string attr_name, XMLOp::XMLValue& value)
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    if(!current_tag_->hasAttribute(QString::fromStdString(attr_name)))
    {
        if(verbose_)
            FERROR("Attribute of name " << attr_name << " of tag " << current_tag_->tagName().toStdString() << " doesn't exist.");
        return false;
    }

    value.set(current_tag_->attribute(QString::fromStdString(attr_name)).toStdString());

    return true;
}

XMLOp::XMLValue XMLOperations::getAttribute(string attr_name)
{
    XMLOp::XMLValue value;

    getAttribute(attr_name, value);    //if fails, value is already init to ""

    return value;
}

bool XMLOperations::getAttributes(map<string,XMLOp::XMLValue>& attributes)
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    QDomNamedNodeMap map_of_attr = current_tag_->attributes();

    for(int i=0;i<(int)map_of_attr.size();i++)
    {
        string name = map_of_attr.item(i).toAttr().name().toStdString();
        XMLOp::XMLValue value = map_of_attr.item(i).toAttr().value().toStdString();

        attributes.insert(pair<string,XMLOp::XMLValue>(name,value));
    }

    return true;
}

bool XMLOperations::getAttributes(map<string,int>& attributes)
{
    map<string,XMLOp::XMLValue> myMap;

    if(!getAttributes(myMap)) return false;

    for(map<string,XMLOp::XMLValue>::iterator itr=myMap.begin(); itr != myMap.end();itr++)
    {
        attributes.insert(pair<string,int>(itr->first, itr->second.toInt()));
    }

    return true;
}

bool XMLOperations::getAttributes(map<string,double>& attributes)
{
    map<string,XMLOp::XMLValue> myMap;

    if(!getAttributes(myMap)) return false;

    for(map<string,XMLOp::XMLValue>::iterator itr=myMap.begin(); itr != myMap.end();itr++)
    {
        attributes.insert(pair<string,double>(itr->first, itr->second.toDouble()));
    }

    return true;
}

bool XMLOperations::getAttributes(map<string,string>& attributes)
{
    map<string,XMLOp::XMLValue> myMap;

    if(!getAttributes(myMap)) return false;

    for(map<string,XMLOp::XMLValue>::iterator itr=myMap.begin(); itr != myMap.end();itr++)
    {
        attributes.insert(pair<string,string>(itr->first, itr->second.toString()));
    }

    return true;
}

vector<string> XMLOperations::getListOfAllElements()
{
    //Let's check, that current tag has been set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    QList<QDomElement> list_of_elements;
    addAllElementsToList(*current_tag_, list_of_elements);

    //convert to vector of strings:
    QVector<QDomElement> vector_of_elements = list_of_elements.toVector();

    vector<string> std_vector;

    for(int i=0;i<vector_of_elements.size();i++)
    {
        std_vector.push_back(vector_of_elements[i].tagName().toStdString());
    }

    return std_vector;
}

vector<string> XMLOperations::getListOfAllElements(vector<string> attributes, bool all_attr_required)
{
    //Let's check, that current tag has been set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    QVector<QString> vector_of_attr;
    for(int i=0;i<(int)attributes.size();i++)
        vector_of_attr.push_back(QString::fromStdString(attributes[i]));
    QList<QDomElement> list_of_elements;
    addAllElementsWithAttrToList(*current_tag_, vector_of_attr, list_of_elements, all_attr_required);

    //convert to vector of strings:
    QVector<QDomElement> vector_of_elements = list_of_elements.toVector();

    vector<string> std_vector;

    for(int i=0;i<vector_of_elements.size();i++)
    {
        std_vector.push_back(vector_of_elements[i].tagName().toStdString());
    }

    return std_vector;
}


void XMLOperations::addAllElementsToList(const QDomElement& elem, QList<QDomElement>& foundElements)
{
    foundElements.append(elem);

    QDomElement child = elem.firstChildElement();
    while( !child.isNull() )
    {
        addAllElementsToList(child, foundElements);
        child = child.nextSiblingElement();
    }

}

void XMLOperations::addAllElementsWithAttrToList(const QDomElement& elem, const QVector<QString>& attr, QList<QDomElement>& foundElements, bool all_attr_required)
{
    if(all_attr_required)
    {
        //check if all attributes are found
        bool ok = true;
        for(int i=0;i<attr.size();i++)
        {
            if(!elem.attributes().contains(attr[i]))
            {
                ok = false;
                break;
            }
        }
        if(ok)
            foundElements.append(elem);
    }
    else
    {
        //check if one of the attributes are found
        for(int i=0;i<attr.size();i++)
        {
            if(elem.attributes().contains(attr[i]))
            {
                foundElements.append(elem);
                break;
            }
        }
    }

    QDomElement child = elem.firstChildElement();
    while( !child.isNull() ) {
        addAllElementsWithAttrToList(child, attr, foundElements, all_attr_required);
        child = child.nextSiblingElement();
    }
}

void XMLOperations::clearTagsContainer()
{
    //delete all pointers in the container:
    for(int i=0;i<(int)tags_container_.size();i++)
    {
        delete tags_container_[i];
    }

    tags_container_.clear();
    tags_container_current_index_ = -1;
}


//Easy access
bool XMLOperations::getValueFromPath(string path, XMLOp::XMLValue& value, bool set_current_tag)
{
    QDomElement temp = *current_tag_;

    if(!goToPath(path))
    {
        if(verbose_)
            FERROR("Couldn't go to path: " << path);
        return false;
    }

    if(!getValue(value))
    {
        if(verbose_)
            FERROR("Failed to get value. " << path << "has children!");
        return false;
    }

    if(!set_current_tag)
    {
        *current_tag_ = temp;
    }

    return true;
}

XMLOp::XMLValue XMLOperations::getValueFromPath(string path, bool set_current_tag)
{
    XMLOp::XMLValue value;

    getValueFromPath(path, value, set_current_tag);

    return value;
}

bool XMLOperations::getValueOfFirstMatch(string tag, XMLOp::XMLValue& value, bool set_current_tag)
{
    return getValueOfMatch(tag, value, 1, set_current_tag);
}

XMLOp::XMLValue XMLOperations::getValueOfFirstMatch(string tag, bool set_current_tag)
{
    XMLOp::XMLValue value;

    getValueOfFirstMatch(tag, value, set_current_tag);

    return value;
}

bool XMLOperations::getValueOfNextMatch(XMLOp::XMLValue& value, bool set_current_tag)
{
    QDomElement temp = *current_tag_;
    int temp2 = tags_container_current_index_;

    if(!goToNextMatch())
    {
        if(verbose_)
            FERROR("Couldn't go the next match! Either we are already at the last match OR no search has been performed!");
        return false;
    }
    value.set(getValue());

    if(!set_current_tag)
    {
        *current_tag_ = temp;
        tags_container_current_index_ = temp2;
    }

    return true;
}

XMLOp::XMLValue XMLOperations::getValueOfNextMatch(bool set_current_tag)
{
    XMLOp::XMLValue value;

    getValueOfNextMatch(value, set_current_tag);

    return value;
}

bool XMLOperations::getValueOfMatch(string tag, XMLOp::XMLValue& value, unsigned int match_number, bool set_current_tag)
{
    QDomElement temp = *current_tag_;
    int temp2 = tags_container_current_index_;

    if(!goToMatch(tag, match_number))
    {
        if(verbose_)
            FERROR("Failed to go to the " << match_number << ". match of tag: " << tag);
        return false;
    }

    if(!getValue(value))
    {
        if(verbose_)
            FERROR("Failed to get value. " << tag << " has children!");
        return false;
    }

    if(!set_current_tag)
    {
        *current_tag_ = temp;
        tags_container_current_index_ = temp2;
    }

    return true;
}

XMLOp::XMLValue XMLOperations::getValueOfMatch(string tag, unsigned int match_number, bool set_current_tag)
{
    XMLOp::XMLValue value;

    getValueOfMatch(tag, value, match_number,set_current_tag);

    return value;
}

bool XMLOperations::getValueOfAllMatches(string tag, vector<XMLOp::XMLValue>& values)
{
    XMLOp::XMLValue value;
    int itr = 1;

    while(getValueOfMatch(tag, value, itr))
    {
        values.push_back(value);
    }

    if(values.size() == 0)
    {
        if(verbose_)
            FERROR("Failed to get any values of matches on tag: " << tag);
        return false;
    }

    return true;
}

vector<XMLOp::XMLValue> XMLOperations::getValueOfAllMatches(string tag)
{
    vector<XMLOp::XMLValue> values;

    getValueOfAllMatches(tag, values);

    return values;
}

bool XMLOperations::getValueOfFirstChild(string child, XMLOp::XMLValue &value)
{
    QDomElement temp = *current_tag_;

    if(!goToFirstChild(child))
    {
        if(verbose_)
            FERROR("Failed to get value of child " << child << ". " << current_tag_->tagName().toStdString() << " has no child of that name!");

        *current_tag_ = temp;
        return false;
    }

    if(!getValue(value))
    {
        if(verbose_)
            FERROR("Failed to get value of child " << child << ".");

        *current_tag_ = temp;
        return false;
    }

    *current_tag_ = temp;

    return true;
}

XMLOp::XMLValue XMLOperations::getValueOfFirstChild(string child)
{
    XMLOp::XMLValue value;

    getValueOfFirstChild(child,value);

    return value;
}

//Adding to the document
bool XMLOperations::addTag(string tag, bool set_current_tag)
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }


    // Fill in the new tag
    QDomElement newTag = dom_document_->createElement(QString::fromStdString(tag));
    current_tag_->appendChild(newTag);

    if(set_current_tag)
    {
        *current_tag_ = newTag;
    }

    return writeFile();
}

bool XMLOperations::addValue(XMLOp::XMLValue value, bool overwrite)
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    //check if current_tag_ has child nodes OR has a value!
    if(current_tag_->hasChildNodes())
    {
        if (current_tag_->childNodes().count() > 1 ||
                current_tag_->firstChild().nodeType() != QDomNode::TextNode)
        {
            FERROR("Attempting to overwrite node '" << current_tag_->tagName().toStdString() << "' even though this has child node(s)!");
            return false;
        }

        if(!overwrite)
        {
            FDEBUG("Overwrite not requested - returning false!");
            return false;
        }

        FDEBUG("Overwrite requested - current value will be erased!");

        //remove current child (values are also considered children!)
        current_tag_->removeChild(current_tag_->firstChild());
    }

    QDomText newValue = dom_document_->createTextNode(QString::fromStdString(value.toString()));
    current_tag_->appendChild(newValue);

    return writeFile();
}

bool XMLOperations::addTagAndValue(string tag, XMLOp::XMLValue value, bool set_current_tag, bool overwrite)
{
    QDomElement temp = *current_tag_;

    if(!addTag(tag,true))
    {
        FDEBUG("Failed to add tag: " << tag << " to " << current_tag_->tagName().toStdString());
        *current_tag_ = temp;
        return false;
    }

    if(!addValue(value,overwrite))
    {
        FDEBUG("Failed to add value: " << value.toString() << " to " << current_tag_->tagName().toStdString());

        if(!set_current_tag) *current_tag_ = temp;
        return false;
    }

    if(!set_current_tag) *current_tag_ = temp;

    return writeFile();
}

bool XMLOperations::addAttribute(string name, XMLOp::XMLValue value, bool overwrite)
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    //Let's check if the attribute already exists:
    //check if current_tag_ has child nodes OR has a value!
    if(current_tag_->hasAttribute(QString::fromStdString(name)))
    {
        FDEBUG(current_tag_->tagName().toStdString() << " already has an attribute of name " << name);

        if(!overwrite)
        {
            FDEBUG("Overwrite not requested - returning false!");
            return false;
        }

        FDEBUG("Overwrite requested - attribute value will be replaced!");
    }

    current_tag_->setAttribute(QString::fromStdString(name),QString::fromStdString(value.toString()));

    return writeFile();
}

bool XMLOperations::addXMLObject(XMLOperations input)
{
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    //ensure input is at root
    input.goToRoot();

    QDomElement fromInput;
    if(!input.getElement(fromInput))
        return false;

    current_tag_->appendChild(fromInput);

    return writeFile();
}

bool XMLOperations::addElement(const QDomElement &input)
{
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    current_tag_->appendChild(input);

    return writeFile();
}


//Deleting in the document
bool XMLOperations::deleteTag()
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root, however, will not set Root and risk deleting the entire document!");
        return false;
    }

    QDomElement markedForDelete = *current_tag_;

    if(!goToParent())
    {
        FDEBUG("Failed to go to parent! Are you sure you are not trying to delete the root node?");
        return false;
    }

    current_tag_->removeChild(markedForDelete);

    return writeFile();
}

bool XMLOperations::deleteChildren()
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root, however, will not set Root and risk deleting the entire document!");
        return false;
    }

    //remove all current children (values are also considered children!)
    while(current_tag_->hasChildNodes())
    {
        current_tag_->removeChild(current_tag_->firstChild());
    }

    return writeFile();
}

bool XMLOperations::deleteAttribute(string name)
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    //Let's check if the attribute already exists:
    //check if current_tag_ has child nodes OR has a value!
    if(!current_tag_->hasAttribute(QString::fromStdString(name)))
    {
        FDEBUG(current_tag_->tagName().toStdString() << " does not have an attribute of name " << name);
    }

    current_tag_->removeAttribute(QString::fromStdString(name));

    return true;
}

QDomDocument XMLOperations::getDocumentObject() const
{
    return *dom_document_;
}

void XMLOperations::setFlag(string flag_name)
{
    //Let's check, that current tag is set:
    if(current_tag_->isNull())
    {
        FDEBUG("Current tag not set. Expecting it to be root!");
        *current_tag_ = dom_document_->documentElement();
    }

    flags_[flag_name] = new QDomElement(*current_tag_);
}

bool XMLOperations::goToFlag(string flag_name)
{
    //check if flag exists:
    if(flags_.find(flag_name) == flags_.end())
    {
        if(verbose_)
            FERROR("Requested to go to flag: " << flag_name << ". However, it doesn't exist / is not set!");
        return false;
    }

    *current_tag_ = *flags_[flag_name];

    return true;
}

bool XMLOperations::goToFlagAndDeleteFlag(string flag_name)
{
    if(!goToFlag(flag_name))
        return false;

    deleteFlag(flag_name);
}

void XMLOperations::deleteFlag(string flag_name)
{
    //check if flag exists:
    if(flags_.find(flag_name) == flags_.end())
    {
        if(verbose_)
            FERROR("Requested to delete flag: " << flag_name << ". However, it doesn't exist / is not set!");
    }

    delete flags_[flag_name];
    flags_.erase(flag_name);
}
