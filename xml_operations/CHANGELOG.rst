^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package xml_operations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.0.1 (2017-05-08)
------------------
* Added CHANGELOG.rst
* Updated package names to comply with ROS conventions
  - file_system_operations
  - logger_sys
  - math_op
  - xml_operations
