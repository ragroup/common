#ifndef XMLOPERATIONS_HPP_
#define XMLOPERATIONS_HPP_

//=================================
// Forward declared dependencies
//=================================
class QDomElement;
class QDomDocument;
class QFile;
class QString;
template <typename T>  class QVector;
template <typename T>  class QList;

//=================================
// Included dependencies
//=================================
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <stdlib.h>
#include <boost/shared_ptr.hpp>

//#include <QDomElement>
//#include <QFile>

namespace XMLOp
{

class XMLValue
{
public:

    XMLValue() : value_("") {}
    XMLValue(std::string value) : value_(value) {}

    XMLValue(int value)
    {
        set(value);
    }

    XMLValue(double value)
    {
        set(value);
    }

    XMLValue(const XMLOp::XMLValue& value_in)
    {
        set(value_in);
    }

    int toInt() const
    {
		  return atoi(value_.c_str());
    }

    double toDouble() const
    {
        return atof(value_.c_str());
    }

    std::string toString() const
    {
        return value_;
    }

    void set(const std::string value)
    {
        value_=value;
    }

    void set(const int value)
    {
        std::stringstream ss;
        ss << value;
        value_ = ss.str();
    }

    void set(const double value)
    {
        std::stringstream ss;
        ss << value;
        value_ = ss.str();
    }

    void set(const XMLValue& value_in)
    {
        value_ = value_in.toString();
    }

    bool operator==(const XMLOp::XMLValue& other)
    { return (this->toString() == other.toString()); }

    bool operator!=(const XMLOp::XMLValue& other)
    { return !(*this == other); }

private:

    std::string value_;

};
}


class XMLOperations{

public:

    XMLOperations(bool verbose = false);

    virtual ~XMLOperations();

    bool openFile(std::string path, std::string file);
    bool openFile(std::string fullFilePath);

    void closeFile();

    //Create new document in memory only
    // File must be stored by calling saveAs with an argument.
    bool createNew(std::string root_tag_name);

    //Create new document and associated file.
    bool createNewFile(std::string full_file_path, std::string root_tag_name);
    bool createNewFile(std::string path, std::string filename, std::string root_tag_name);

    bool deleteFile(std::string full_file_path = "");       //Deletes the specified file. If no file is specified, it will delete the current file
    bool deleteFile(std::string path, std::string filename);

    bool clearFile(void);

    //Save the current file as a new name
    //Will create the associated file in the filesystem.
    bool saveAs(std::string full_file_path, bool overwrite = false);
	 bool saveAs(std::string path, std::string filename, bool overwrite = false);

    //Returns true if the current document is associated with a file
    // (hence is saved to the file as XMLOper auto saves on edits)
    bool isSaved();

	 // Get filename
	 std::string getFilenameFull(void) const;
	 std::string getFilename(void) const;

    //Functions to navigate the document and the search
    bool goToFirstMatch(std::string tag);                   //Search document for "tag" and go to first occurance   (PLEASE NOTE: As from 26-02-2015 it also counts in the root tag if it matches!)
    bool goToNextMatch();                                   //Go to next occurance (goToFirstMatch must be called first!)
    bool goToMatch(std::string tag, unsigned int number);   //Search document and go to the "number"^th occurance. (starts at 1)
    bool goToPath(std::string path);                        //Go to a specified "path" (parent child divided by "/")
    bool goToFirstChild(std::string child = "");            //Go to the first child (of name)
    bool goToNextSibling(std::string sibling = "");         //Go to the next sibling (of name)
    bool goToParent();                                      //Go to parent
    bool goUp(int steps);                                   //Go to parent 'steps' times
    bool goToRoot();                                        //Go to root element of the document
    bool goToTag(QDomElement* tag);                         //Go to specific element (e.g. from getCurrentTag)


    //functions to check, count in the document
    unsigned int countOccurances(std::string tag);          //Returns the number of times a tag is found in the entire document

    bool hasAttribute(std::string attr_name);                    //check if current tag has attribute


    //Functions to retrieve value/tag
    bool getTagName(std::string& tag);                      //Get the tag name
    std::string getTagName(void);

    bool getValue(XMLOp::XMLValue& value);                  //Get the value of a tag (given it doesn' have children)
    XMLOp::XMLValue getValue();                             //Get value of XMLValue type, which later can be called int/double/string on

    bool getElement(std::string& element);                  //Get an element, hence structure of a tag (given it has children)
    bool getElement(QDomElement& element);

    bool getChildren(std::map<std::string, XMLOp::XMLValue> &children); //Get a map of the children (tag name and value)
    bool getChildren(std::map<std::string, std::string> &children);
    bool getChildren(std::map<std::string, int> &children);
    bool getChildren(std::map<std::string, double> &children);
	 std::string getParentTagName();
	 QDomElement* getCurrentTag() { return current_tag_; }


    bool getChildrenValues(std::vector<XMLOp::XMLValue> &children_values);
    bool getChildrenValues(std::vector<std::string> &children_values);

    int getNumberOfChildren();

    bool getAttribute(std::string attr_name, XMLOp::XMLValue& value);
    XMLOp::XMLValue getAttribute(std::string attr_name);

    bool getAttributes(std::map<std::string,XMLOp::XMLValue>& attributes);
    bool getAttributes(std::map<std::string,int>& attributes);
    bool getAttributes(std::map<std::string,double>& attributes);
    bool getAttributes(std::map<std::string,std::string>& attributes);

    //get a list of all elements starting from "current tag".
    std::vector<std::string> getListOfAllElements();
    //get a list of all elements starting from "current tag" with the attributes specified.
    std::vector<std::string> getListOfAllElements(std::vector<std::string> attributes, bool all_attr_required = true);

    //Easy access:
    bool getValueFromPath(std::string path, XMLOp::XMLValue& value, bool set_current_tag=true);
    XMLOp::XMLValue getValueFromPath(std::string path, bool set_current_tag=true);

    ///Get the value of the first element in the document with tag-name == <tag>. Note: Searches from root - NOT from current_tag_.
    bool getValueOfFirstMatch(std::string tag, XMLOp::XMLValue& value, bool set_current_tag=true);
    XMLOp::XMLValue getValueOfFirstMatch(std::string tag, bool set_current_tag=true);

    bool getValueOfNextMatch(XMLOp::XMLValue &value, bool set_current_tag=true);
    XMLOp::XMLValue getValueOfNextMatch(bool set_current_tag=true);

    bool getValueOfMatch(std::string tag, XMLOp::XMLValue& value, unsigned int match_number=1, bool set_current_tag=true);
    XMLOp::XMLValue getValueOfMatch(std::string tag, unsigned int match_number, bool set_current_tag);

    bool getValueOfAllMatches(std::string tag, std::vector<XMLOp::XMLValue>& values);
    std::vector<XMLOp::XMLValue> getValueOfAllMatches(std::string tag);

    ///Get the value of the first child element with tag-name <child>
    bool getValueOfFirstChild(std::string child, XMLOp::XMLValue& value);
    XMLOp::XMLValue getValueOfFirstChild(std::string child);

    //adding
    bool addTag(std::string tag, bool set_current_tag=true);

    bool addValue(XMLOp::XMLValue value, bool overwrite=false);
    bool addValue(int value, bool overwrite=false) {return addValue(XMLOp::XMLValue(value), overwrite);}
    bool addValue(double value, bool overwrite=false){return addValue(XMLOp::XMLValue(value), overwrite);}
    bool addValue(std::string value, bool overwrite=false){return addValue(XMLOp::XMLValue(value), overwrite);}

    bool addTagAndValue(std::string tag, XMLOp::XMLValue value, bool set_current_tag=true, bool overwrite=false);
    bool addTagAndValue(std::string tag, int value, bool set_current_tag=true, bool overwrite=false) {return addTagAndValue(tag, XMLOp::XMLValue(value),set_current_tag, overwrite);}
    bool addTagAndValue(std::string tag, double value, bool set_current_tag=true, bool overwrite=false) {return addTagAndValue(tag, XMLOp::XMLValue(value),set_current_tag, overwrite);}
    bool addTagAndValue(std::string tag, std::string value, bool set_current_tag=true, bool overwrite=false) {return addTagAndValue(tag, XMLOp::XMLValue(value),set_current_tag, overwrite);}

    bool addAttribute(std::string name, XMLOp::XMLValue value, bool overwrite = false);
    bool addAttribute(std::string name, std::string value, bool overwrite = false) {return addAttribute(name, XMLOp::XMLValue(value), overwrite);}
    bool addAttribute(std::string name, double value, bool overwrite = false) {return addAttribute(name, XMLOp::XMLValue(value), overwrite);}
    bool addAttribute(std::string name, int value, bool overwrite = false) {return addAttribute(name, XMLOp::XMLValue(value), overwrite);}

    bool addXMLObject(XMLOperations input);
    bool addElement(const QDomElement &input);

    //deleting
    bool deleteTag();                                       //Delete current tag and all children
    bool deleteChildren();                                  //Delete all children/values of current tag
    bool deleteAttribute(std::string name);

    //returns a copy of the entire domDocument. This is safe as the domDocument doesn't link to the specific file AND it is JUST a COPY.
    QDomDocument getDocumentObject() const;

    //set and goTo flags:
    void setFlag(std::string flag_name);
    bool goToFlag(std::string flag_name);
    void deleteFlag(std::string flag_name);
    bool goToFlagAndDeleteFlag(std::string flag_name);

private:

	 void initialize(std::string root_tag_name);					// Initializes content. Does not write to file.

    bool writeFile();                                       //Write to the file. This is called automatically after making a change!

    bool findTags(std::string tag);

    //Adds all elements/tags from a given element and "down" to a list.
    //This function is used in "getListOfAllElements"
    void addAllElementsToList(const QDomElement& elem, QList<QDomElement>& foundElements);

    //Adds all elements/tags from a given element and "down" with the specified "attributes" to a list. Similar to the function above
    //This function is also used in "getListOfAllElements"
    void addAllElementsWithAttrToList(const QDomElement& elem, const QVector<QString>& attr, QList<QDomElement>& foundElements, bool all_attr_required);

    void clearTagsContainer();

	 //stuff
    QFile* xml_file_;
    QDomDocument* dom_document_;
    QDomElement* current_tag_;
    std::vector<QDomElement*> tags_container_;
    int tags_container_current_index_;
    bool verbose_;
    std::map<std::string, QDomElement*> flags_;

};



#endif /* XMLOPERATIONS_HPP_ */
