#ifndef MathOperations_HPP_
#define MathOperations_HPP_

//=================================
// Forward declared dependencies
//=================================


//=================================
// Included dependencies
//=================================
#include "eigen3/Eigen/Dense"
#include <opencv2/core/core.hpp>
#include <tf2/LinearMath/Transform.h>
#include "boost/array.hpp"
#include "geometry_msgs/Transform.h"
#include "geometry_msgs/TransformStamped.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseStamped.h"

namespace MathOp
{
enum rotation
{
    AXISANGLE,
    ZYX_EULER,
    ZXZ_EULER
};

class Transform
{
public:
	// Constructors:
	Transform();
	Transform(const Eigen::Matrix4d& matrix) : matrix_(matrix) {}
	Transform(const tf2::Transform& matrix);
	Transform(const cv::Mat& matrix);
	Transform(const cv::Matx44d& matrix);
	Transform(const boost::array<float, 16> matrix);
	Transform(const boost::array<float, 12> matrix);
    Transform(double x, double y, double z, double Ra, double Rb, double Rc, MathOp::rotation rot_type);
    Transform(double x, double y, double z, double Ra, double Rb, double Rc);
    Transform(const cv::Point3d trans, const cv::Vec3d rot, MathOp::rotation rot_type = MathOp::ZYX_EULER);
    Transform(const cv::Vec3d trans, const cv::Vec3d rot);
    Transform(const cv::Vec3d trans, const cv::Vec3d rot, MathOp::rotation rot_type);
    Transform(const Eigen::Vector3d translation, const Eigen::Vector3d rot);
	Transform(const cv::Vec6d vec);	// Note: Order is x, y, z, A, B, C
    Transform(const cv::Vec3d trans, const Eigen::Matrix3d& matrix);
	Transform(const geometry_msgs::Transform transform);
	Transform(const geometry_msgs::Pose pose);
	Transform(double x, double y, double z, double rw, double rx, double ry, double rz);
	Transform(std::vector<double> quaternion_frame);

	// Rotates about vector placed in origin and rota:
    Transform       setRotationAboutOriginVector(cv::Vec3d vec, double angle);
    Transform       setRotationAboutOriginVector(cv::Vec3d vec, double angle, cv::Point3d trans);
    Transform       rotateAroundZ(double angle);

    Transform&      setTranslation(double x, double y, double z);
    Transform&      translate(double x, double y, double z);
    Transform       inverse() const;
    Transform       convertToMeters() const;
    Transform       convertToMm() const;

	// Check data:
    bool            isIdentity() { return matrix_.isIdentity(); }

    // Relations:
    double          calcDist(const MathOp::Transform& rhs) const;
    cv::Vec3d       calcDistVecFrom(const MathOp::Transform& rhs) const;    // This_pos minus rhs_pos

	// Get data:
    cv::Vec3d           getEuler() const;		// [rad]
    cv::Vec3d           getEuler(MathOp::rotation rot) const;		// [rad]
    cv::Vec3d           getTranslation() const;
    cv::Vec4d           getQuaternions() const;
    cv::Vec3d           getBasisX() const { return cv::Vec3d( matrix_(0,0), matrix_(1,0), matrix_(2,0) ); }
    cv::Vec3d           getBasisY() const { return cv::Vec3d( matrix_(0,1), matrix_(1,1), matrix_(2,1) ); }
    cv::Vec3d           getBasisZ() const { return cv::Vec3d( matrix_(0,2), matrix_(1,2), matrix_(2,2) ); }
    cv::Vec6d           getVector(bool degrees = false) const;	// Note: Order is x, y, z, A, B, C
    cv::Vec6d           getVector(MathOp::rotation rot) const;
	std::vector<double> getStdVector(bool degrees = false) const;
	std::vector<double> getFullQuanternionTFVector();
	geometry_msgs::Transform getGeometryMsgsTransform() const;
	geometry_msgs::TransformStamped getGeometryMsgsTransformStamped() const;
	geometry_msgs::Pose getGeometryMsgsPose() const;
    geometry_msgs::PoseStamped getGeometryMsgsPoseStamped() const;
    tf2::Transform      getTf2Transform() const;
    void                getGlArray(double ret_val[16]) const;
    Eigen::Matrix4d     getEigen() { return matrix_; }

	// Input/otput:
    std::string         print() const;
    std::string         printVector() const;	// Note: Order is x, y, z, A, B, C
    std::string         printInfo() const;	// Note: Order is x, y, z, A, B, C || z_x, z_y, z_z
    int                 saveTo(std::string path, int rows = 4, int cols = 4) const;
    int                 loadFrom(std::string path);

	// Overloaded operators:
	const Transform operator*(const Transform& rhs) const;
	const cv::Vec3d operator*(const cv::Vec3d& rhs) const;
	const cv::Point3d operator*(const cv::Point3d& rhs) const;
	const cv::Vec4d operator*(const cv::Vec4d& rhs) const;
	const Transform operator+(const cv::Vec3d& rhs) const;
	const Transform operator-(const cv::Vec3d& rhs) const;
	double operator()(const int& nRow, const int& nCol) const;

private:
    void init(const Eigen::Matrix3d& matrix, double x, double y, double z);
	void init(double Ra, double Rb, double Rc, double x, double y, double z, rotation rot);

    Eigen::Matrix4d matrix_;
};

inline double Transform::operator()(const int& nRow, const int& nCol) const
{
//	 assert(nCol >= 0 && nCol < 4);
//	 assert(nRow >= 0 && nRow < 4);

	 return matrix_(nRow,nCol);
}


class Vector
{
public:
//	Vector(double x, double y, double z) : vec_(x, y, z) {}
//	Vector(cv::Vec3d v) : vec_(v[0], v[1], v[2]) {}

//	Vector dot(const Vector& v) const;
//	Vector cross(const Vector& v) const;
//	double operator[](const int element) const;

	static cv::Vec3d CrossProduct(cv::Vec3d vec1, cv::Vec3d vec2);
	static double DotProduct(cv::Vec3d vec1, cv::Vec3d vec2);

private:
//	cv::Vec3d vec_;
};

double findAngle(const cv::Vec3d& v1, const cv::Vec3d& v2);

}


#endif /* MathOperations_HPP_ */
